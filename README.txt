
Notifications Tools: README.txt
===============================

This is a collection of modules and tools for Messaging and Notifications, intended for high traffic/high volume web sites. They provide logging, queue processing with Drush, multiple worker threads with Drupal Queue, and some other options for fine tuning composition and message sending background processing.

IMPORTANT: There are no new features here, just some tools to manage high loads of Notifications and Messages. If your site is working fine without these modules, *you don't need them*.
